# Oposiciones 2018

Repositorio para presentaciones, katas, código y demás de la programación

## URLS

* [Desarrollo con Javascript](https://gitpitch.com/alvaro_maceda/oposiciones_2019/master?grs=bitbucket&p=Desarrollo_con_Javacript)

## Gitpitch

Para crear los enlaces a las presentaciones se utilizan URL de este tipo:
```https://gitpitch.com/user/repo/branch?grs=service&p=directory```

Hay que cambiar grs por el servicio que se está utilizando: github, gitlab o bitbucket.

Por ejemplo, si quisiésemos mostrar la presentación de un tema en el directorio "Tema_Tres" 
de un repositorio de Bitbucket, la URL sería:
```https://gitpitch.com/alvaro_maceda/oposiciones_2018/master?grs=bitbucket&p=Tema_Tres```

**OJO**, porque los espacios en directorios no funcionan correctamente (08/2018)

Para acceder al PITCHME de la raíz del repositorio, son las mismas URL pero sin incluir
el parámetro "p". Por ejemplo: ```https://gitpitch.com/alvaro_maceda/oposiciones_2018/master?grs=bitbucket``` 

Hay una versión desktop que habrá que suscribir de cara a las oposiciones para  no depender de 
conectividad Internet en la presentación. 

## Comandos útiles

[Preparar un comando para servir con browse reload](https://medium.com/@svinkle/start-a-local-live-reload-web-server-with-one-command-72f99bc6e855)
npm install -g browser-sync
browser-sync start -s -f . --no-notify --host localhost --port 9000 --no-open

Se puede descargar una presentación gitpitch desde el menú cuando se está viendo


Recursos gitpitch:
- [Documentacion](https://gitpitch.com/docs/about/)
- [Presentacion en local](https://github.com/gitpitch/gitpitch/wiki/Developing-Testing-Locally)
